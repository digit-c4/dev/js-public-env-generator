const fs = require("fs");
const path = require("path");
const os = require("os");

const readInput = require("../src/readInput");

test("read input relative path", () => {
  expect(readInput("./test/unit/input-file.txt")).toEqual("blabetiblou\r\n");
});

test("read absolute path", () => {
  expect(
    readInput(path.join(__dirname, "../test/unit/input-file.txt"))
  ).toEqual("blabetiblou\r\n");
});

test("read tmp file", () => {
  const tmpDir = os.tmpdir();
  const tmpFile = "test-js-public-env-generator-" + new Date().getTime()
  const tmpFilePath = path.join(
    tmpDir,
    tmpFile,
  );
  fs.writeFileSync(tmpFilePath, "blabetiblou");
  const cwd = process.cwd()
  process.chdir(tmpDir)
  expect(readInput(`./${tmpFile}`)).toEqual("blabetiblou");
  process.chdir(cwd)
});
