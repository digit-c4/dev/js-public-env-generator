const util = require('util');
const exec = util.promisify(require('child_process').exec);

test("Install v0 branch from code.europa", () => {
  return exec("docker run node npm install -g git+https://code.europa.eu/digit-c4/dev/js-public-env-generator#v0")
    .then((r) => {
      expect(r).not.toBeNull();
    })
}, 10_000);

test("Install locally and check cli help", () => {
  return exec("docker run -v $(pwd):/library node /bin/bash -c \"npm install -g /library && js-public-env-generator -h\"")
    .then((r) => {
      expect(r).not.toBeNull();
    })
}, 10_000);
