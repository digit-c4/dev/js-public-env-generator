const dotenv = require("dotenv");
const path = require("path");

const loadEnv = require("../src/loadEnv");

//* Setting up test env and data
const rootPath = path.resolve(__dirname, "..");
dotenv.config({ path: path.join(rootPath, "/test/test.env") });
const testData = {
  EU_LOGIN_CLIENT_ID: "00000-00000-00000-00000-00000",
  BACKEND_API_HOST: "https://my-backend.com:8080",
};
const testData_edgeCase_1 = {
  EU_LOGIN_CLIENT_ID: "00000-00000-00000-00000-00000",
  BACKEND_API_HOST: "https://my-backend.com:8080",
  BACKEND_API_TEST: "https://my-test.com:8080",
};
const testData_edgeCase_2 = {
  EU_LOGIN_CLIENT_ID: "00000-00000-00000-00000-00000",
  BACKEND_API_HOST: "https://my-backend.com:8080",
  BACKEND_API_TEST: "",
};
const testData_edgeCase_3 = {
  EU_LOGIN_CLIENT_ID: "00000-00000-00000-00000-00000",
  BACKEND_API_HOST: "https://my-backend.com:8080",
  BACKEND_API_TEST: " ",
};

test("Simple successful test", () => {
  expect(loadEnv({ EU_LOGIN_CLIENT_ID: "", BACKEND_API_HOST: "" })).toEqual(
    testData
  );
});
test("Successful test with edge case (key is not in env, but in env file)", () => {
  expect(
    loadEnv({
      EU_LOGIN_CLIENT_ID: "",
      BACKEND_API_HOST: "",
      BACKEND_API_TEST: "https://my-test.com:8080",
    })
  ).toEqual(testData_edgeCase_1);
});
test("Successful test with edge case (key is not in env and not in env file)", () => {
  expect(
    loadEnv({
      EU_LOGIN_CLIENT_ID: "",
      BACKEND_API_HOST: "",
      BACKEND_API_TEST: "",
    })
  ).toEqual(testData_edgeCase_2);
});
test("Successful test with edge case with blank space value", () => {
  expect(
    loadEnv({
      EU_LOGIN_CLIENT_ID: "",
      BACKEND_API_HOST: "",
      BACKEND_API_TEST: " ",
    })
  ).toEqual(testData_edgeCase_3);
});
test("Error due to null", () => {
  expect(() => loadEnv(null)).toThrow();
});
test("Error due to wrong type of arg", () => {
  expect(() =>
    loadEnv([
      {
        EU_LOGIN_CLIENT_ID: "",
        BACKEND_API_HOST: "",
        BACKEND_API_TEST: "",
      },
    ])
  ).toThrow();
});

test("missing env and empty default value", () => {
  expect(
    loadEnv({
      TEST_EMPTY: "",
    })
  ).toEqual({ TEST_EMPTY: "" });
});

test("missing env and blank default value", () => {
  expect(
    loadEnv({
      TEST_BLANK: " ",
    })
  ).toEqual({ TEST_BLANK: " " });
});

test("missing env and null default value", () => {
  expect(
    loadEnv({
      TEST_NULL: null,
    })
  ).toEqual({ TEST_NULL: null });
});

test("Available env and null default value", () => {
  process.env["TEST_VALUE1"] = "a";
  expect(
    loadEnv({
      TEST_VALUE1: null,
    })
  ).toEqual({ TEST_VALUE1: "a" });
});

test("Available env and null default value", () => {
  process.env["TEST_VALUE2"] = "a";
  expect(
    loadEnv({
      TEST_VALUE2: "",
    })
  ).toEqual({ TEST_VALUE2: "a" });
});

test("Available empty env and null default value", () => {
  process.env["TEST_VALUE3"] = "";
  expect(
    loadEnv({
      TEST_VALUE3: null,
    })
  ).toEqual({ TEST_VALUE3: "" });
});

test("Available empty env and empty default value", () => {
  process.env["TEST_VALUE4"] = "";
  expect(
    loadEnv({
      TEST_VALUE4: "",
    })
  ).toEqual({ TEST_VALUE4: "" });
});

test("Available empty env and non empty default value", () => {
  process.env["TEST_VALUE5"] = "";
  expect(
    loadEnv({
      TEST_VALUE5: "blabetiblou",
    })
  ).toEqual({ TEST_VALUE5: "" });
});

test("empty requirement", () => {
  expect(loadEnv({})).toEqual({});
});
