# js-public-env-generator

## Usage

```sh
# Have some environment variables set
export EU_LOGIN_CLIENT_ID=ffe4a977-70fa-480e-89f1-044e2bd99d57
export ULTRA_SECRET_PASSWORD=pas5W0rD

# declare the public environment variables in a .env-alike file
cat <<EOT >> .env.public
EU_LOGIN_CLIENT_ID=
BACKEND_API_HOST=https://default-host.com:8080
DB_PORT=
DB_HOST=""
EOT

# Install this lib
npm install -g git+https://code.europa.eu/digit-c4/dev/js-public-env-generator

# Run the cli
js-public-env-generator ./directory/.env.public.js

# Watch the outcome
cat ./directory/.env.public.js
NMS_ENV = {
    "EU_LOGIN_CLIENT_ID": "ffe4a977-70fa-480e-89f1-044e2bd99d57",
    "BACKEND_API_HOST": "https://default-host.com:8080",
    "DB_PORT": null,
    "DB_HOST": "",
}
```

## Test
```shell
npm run test
```
