#!/usr/bin/env node
const fs = require("fs");
const argparse = require("argparse");
const path = require("path");

const dotenv = require("dotenv");

const loadEnv = require("./loadEnv");
const readInput = require("./readInput");

const parser = new argparse.ArgumentParser({
  description: "JS env generator.",
});

parser.addArgument(["-i", "--input"], {
  defaultValue: path.join(__dirname, ".env.public"),
  help: "Path of your file in which you need to give the variables you want",
});
parser.addArgument(["output"], {
  nargs: "?",
  defaultValue: path.join(__dirname, ".env.public.json"),
  help: "Path of your output file",
});

const args = parser.parseArgs();

if (Object.keys(args).length !== 2) {
  console.error(
    `Wrong argument number. 2 argument expected, ${
      Object.keys(args).length - 2
    } were given... `
  );
  process.exit(1);
}

let outputPath = "";
if (path.isAbsolute(args.output)) {
  outputPath = args.output;
} else {
  outputPath = path.join(__dirname, args.output);
}

let parsed = readInput(args.input);

const config = dotenv.parse(parsed);
const loadRes = loadEnv(config);

fs.writeFileSync(outputPath, JSON.stringify(loadRes, null, 2));
console.log("Exiting with success");
process.exit(0);
