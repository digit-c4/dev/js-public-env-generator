/* ReadInput take one argument : input (-i arg from cli) and return the content of the file */
const fs = require("fs");

function readInput(input) {
  try {
    let content = fs.readFileSync(input, {
      encoding: "utf8",
    });
    return content;
  } catch (error) {
    throw new Error(`Error during reading of file ${input}: ${error}`);
  }
}

module.exports = readInput;
