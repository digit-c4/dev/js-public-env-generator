/**
 * Takes a default string key-value environment object and returns the
 * same environment with the actual environment value if available.
 */
function loadEnv(obj) {
  if (typeof obj !== "object" || Array.isArray(obj)) {
    throw new Error(`${obj} is not of type object...`);
  } else if (Object.keys(obj).length <= 0) {
    return {};
  } else {
    let target = "";
    let returnObject = {};
    Object.keys(obj).forEach((variable) => {
      target = process.env[variable];

      returnObject[variable] = target !== undefined ? target : obj[variable];
    });
    return returnObject;
  }
}

module.exports = loadEnv;
